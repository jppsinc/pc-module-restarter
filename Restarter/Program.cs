﻿using System.Diagnostics;
using System.Linq;

namespace Restarter
{
    class Program
    {   // Created: 2017.07.24

        static void Main(string[] args)
        {
            string parameters = "-restart";

            if (args.Length < 1)
            {
                return;
            }
            else if (!args[0].Equals("--go"))
            {
                return;
            }
            else
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (args[i].Equals("-dev"))
                    {
                        parameters += " -dev";
                        continue;
                    }

                    if (args[i].Equals("-support"))
                    {
                        parameters += " -support";
                        continue;
                    }

                    if (args[i].Equals("-noupdate"))
                    {
                        parameters += " -noupdate";
                        continue;
                    }
                }

                System.Console.WriteLine(parameters); // FOR DEBUG

                string processName = "rcpcmodule";
                string executeName = string.Format("{0}.exe", processName);

                System.Console.WriteLine(executeName); // FOR DEBUG

                var process = Process.GetProcessesByName(processName);

                if (process.Count() > 0)
                {
                    if (process[0].WaitForExit(3000))
                    {
                        Process.Start(executeName, parameters);
                    }
                }
                else
                {
                    if (System.IO.File.Exists(executeName))
                    {
                        Process.Start(executeName, parameters);
                    }
                    else
                    {
                        System.Console.WriteLine("NO_FILE"); // FOR DEBUG
                    }
                }
            }
        }
    }
}